<link rel="stylesheet" href="styles/header.css">

<header class="header">
    <nav class="navbar">
        <h1>SD eCommerce</h1>

        <ul class="navbar-links">
            <li>
                <a>Home</a>
            </li>
            <li>
                <a>Shop</a>
            </li>
            <li>
                <a>About</a>
            </li>
            <li>
                <a>Login</a>
            </li>
        </ul>
    </nav>

</header>
