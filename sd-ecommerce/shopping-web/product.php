<link rel="stylesheet" href="styles/product.css">

<div class="product">
    <div class="col-product-img">
        <?php
        $product_id = $_GET['id'];
        $stmt = $pdo->prepare('SELECT * FROM products WHERE product_id = ' . $product_id . ' LIMIT 1');
        $stmt->execute();
        $product = $stmt->fetch(PDO::FETCH_ASSOC);
        ?>
        <img src="assets/images/<?= $product['image_thumbnail'] ?>" alt="<?= $product['product_name'] ?>">
    </div>
    <div class="col-product-details">
        <h1><?= $product['product_name'] ?></h1>
        <p class="product-desc"><?= $product['description'] ?></p>
        <div>
            <span>&dollar;<?= $product['unit_price'] ?></span>
        </div>
        <div>
            <button class="btn-add-to-cart">Add to Cart</button>
        </div>
    </div>
</div>
