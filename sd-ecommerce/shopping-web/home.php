<link rel="stylesheet" href="styles/home.css">

<h2>Newly Added Products</h2>
<div class="product-grid">
    <?php
    // Get the 4 most recently added products
    $stmt = $pdo->prepare('SELECT * FROM products LIMIT 9');
    $stmt->execute();
    $products = $stmt->fetchAll(PDO::FETCH_ASSOC);
    ?>
    <?php foreach ($products as $product): ?>
        <a href="index.php?page=product&id=<?=$product['product_id']?>" class="product">
            <img src="assets/images/<?= $product['image_thumbnail'] ?>" alt="<?= $product['product_name'] ?>">
            <div class="product-details">
                <span class="name"><?=$product['product_name']?></span>
                <span class="price">
                &dollar;<?=$product['unit_price']?>
                    <?php if (isset($product['rrp'])): ?>
                        <?php if ($product['rrp'] > 0): ?>
                            <span class="rrp">&dollar;<?=$product['rrp']?></span>
                        <?php endif; ?>
                    <?php endif; ?>
            </span>
            </div>
        </a>
    <?php endforeach; ?>
</div>
