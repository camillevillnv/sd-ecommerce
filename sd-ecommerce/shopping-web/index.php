<?php
include './config/config_global.php';

define('BASE_PATH', dirname(__FILE__) . '/');
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,minimum-scale=1">
        <title>SD eCommerce</title>
        <link rel="stylesheet" href="styles/styles.css">
    </head>
    <body>
        <?php include './layout/header.php'; ?>

        <main>
            <?php include './config/routing.php'; ?>
        </main>

        <?php include './layout/footer.php'; ?>
    </body>
</html>
